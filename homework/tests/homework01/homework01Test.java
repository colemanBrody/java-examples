package homework01;

/*
    Brody Coleman
    Whatcom Community College
    CS145
    09/26/2018

    Tests for review problems from Building Java Programs book.
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class homework01Test {
    private homework01 myWork = new homework01();

    @org.junit.jupiter.api.Test
    void testSum() {
        int[] nums = {-5, 234, -23};
        assertEquals(206, myWork.sum(nums));
        assertEquals(myWork.sum(nums), myWork.sum(nums));
        int[] nums2 = {0};
        assertEquals(0, myWork.sum(nums2));
    }

    @org.junit.jupiter.api.Test
    void testFailSum(){
        int[] data = {4,5,6,7};
        assertEquals(4, myWork.sum(data));
    }

    @Test
    void sequenceNumbers() {
        assertEquals("1 4 9 16 25 36 49 64 81 100 ", myWork.sequenceNumbers());
    }

    @Test
    void diagonalNumbers() {
        assertEquals("    1\n   2\n  3\n 4\n5", myWork.diagonalNumbers());
    }

    @Test
    void diagonalStair() {
        assertEquals("   1\n   22\n  333\n 4444\n55555", myWork.diagonalStair());
    }

    @Test
    void vertical() {
        assertEquals("t\ne\ns\nt\n", myWork.vertical("test"));
        assertEquals("f\nr\ne\ne\nd\no\nm\n", myWork.vertical("freedom"));
        assertEquals("", myWork.vertical(""));
        assertEquals("h\ni\n\n\nh\ni\n", myWork.vertical("hi\nhi"));
    }

    @Test
    void heronsTriangleFormula() {
        assertEquals(17.41228014936585, myWork.heronsTriangleFormula(5, 7, 9));
        assertEquals(0.49999999999999983, myWork.heronsTriangleFormula(1, 1, Math.sqrt(2)));
    }

    @Test
    void printRange() {
        assertEquals("5 6 7 8 9 10 11", myWork.printRange(5, 11));
        assertEquals("4 3 2 1 0 -1 -2", myWork.printRange(4, -2));
        assertEquals("0", myWork.printRange(0, 0));
    }

    @Test
    void findLongestString() {
        String[] searchy = {"first", "another", "test", "where", "apples"};
        assertEquals("another", myWork.findLongestString(searchy));

        String[] moreStrings = {"1", "test", null , "longest", "s"};
        assertEquals("longest", myWork.findLongestString(moreStrings));
    }

    @Test
    void canyonYell() {
        assertEquals("8899hhii", myWork.canyonYell("89hi", 2));
        assertEquals("", myWork.canyonYell("hi there", 0));
    }

    @Test
    void hasMidPoint() {
        assertTrue(myWork.hasMidPoint(1, 2, 3));
        assertTrue(myWork.hasMidPoint(1, 1, 1));
        assertTrue(myWork.hasMidPoint(5, 7, 9));
        assertFalse(myWork.hasMidPoint(1, 1, 2));
    }

    @Test
    void digitRange() {
        assertEquals(7, myWork.digitRange(63520435));
        assertEquals(4, myWork.digitRange(978697869));
    }

    @Test
    void isAllVowels() {
        assertFalse(myWork.isAllVowels("oaioifuoaioifgekjv"));
        assertTrue(myWork.isAllVowels("aeiioou"));
    }

    @Test
    void countFileWordsOfLength() {
        /*
        TODO I will be leaving this one for you to test.
         */
    }
}