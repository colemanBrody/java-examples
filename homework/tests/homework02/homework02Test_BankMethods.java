package homework02;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertEquals;

class homework02Test_BankMethods {

    private BankAccount debitAccount;
    @BeforeEach
    void setUp() {
        debitAccount = new BankAccount("Mr.", "Tester");
        debitAccount.deposit(674.23);
    }

    @Test
    void buildMyAccount() {
        assertEquals(debitAccount, homework02.buildMyAccount("Mr.", "Tester"));
    }

    @Test
    void checkBalance() {
        assertEquals(674.23, debitAccount.checkBalance());
    }

    @Test
    void deposit() {
        debitAccount.deposit(1111.11);
        assertEquals(1785.34, debitAccount.checkBalance());
    }

    @Test
    void withdraw() {
        debitAccount.withdraw(100.10);
        assertEquals(574.13, debitAccount.checkBalance());
    }

    @Test
    void transfer() {
        BankAccount transferToAccount = new BankAccount("I", "Received");
        transferToAccount.deposit(8.10);

        debitAccount.transfer(transferToAccount, 544.13);

        assertEquals(130.10, debitAccount.checkBalance());
        assertEquals(552.23, transferToAccount.checkBalance());
    }

    @Test
    void printString() {
       assertEquals("(The Account of Mr. Tester has a balance of $674.23)", debitAccount.toString());
    }
}