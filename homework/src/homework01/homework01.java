package homework01;

/*
    Name -
    Class -
    Date -

    Description -
 */

public class homework01 {
    /**
     * Add the numbers in the nums array.
     *
     * Use a loop of some sort to sum up the numbers in the array nums.
     *
     * @param nums array containing the numbers to sum
     * @return a value that is the sum of the nums.
     */
    public int sum(int[] nums){
        int total = 0;
        for(int i = 0; i < nums.length; i++){
            total += nums[i];
        }
        return total;
    }

    /**
     * Write a no parameter method that will use a loop to return the following sequence:
     * '1 4 9 16 25 36 49 64 81 100' as a string.
     * Try not to use multiplication.
     *
     * @return the requested string.
     */
    public String sequenceNumbers(){
        return "1 4 9 16 25 36 49 64 81 100";
    }

    /**
     * Write a method which will use nested for loops to create the following figure:
     *
     *     1
     *    2
     *   3
     *  4
     * 5
     *
     * @return the requested string.
     */
    public String diagonalNumbers(){
        String returnString = "";
        for(int i = 1; i <= 5; i++){
             int spaces = 5 - i;
             for(int j = 0; j < spaces; j++){
                 returnString += " ";
             }
             returnString += i + "\n";
        }
        return returnString;
    }

    /**
     * Write a method with nested for loops to create the following figure:
     *
     *     1
     *    22
     *   333
     *  4444
     * 55555
     *
     * @return the requested figure.
     */
    public String diagonalStair(){
        return "    1\n   22\n  333\n 4444\n55555";
    }


    /**
     * Write a method called vertical which takes a string as a parameter and returns
     * a string that contains each character of the original string on their own lines.
     *
     * Example:
     * vertical("test")
     *
     * would produce the string:
     * "t
     *  e
     *  s
     *  t"
     *
     * @param s string to verticalize.
     * @return a string containing the strings character one character a line.
     */
    public String vertical(String s){
        return "";
    }

    /**
     * herons formula is:
     * tarea = sqroot( s * (s - a) * (s - b) * (s - c))
     * where s is the value s  = (a + b + c) /2
     *
     * Use these formula to write a method to calculate the area of a triangle
     * given three sides.
     *
     * @param a first side.
     * @param b second side.
     * @param c third side.
     * @return the computed area using herons formula.
     */
    public double heronsTriangleFormula(double a, double b, double c){
        return 5.6;
    }

    /**
     * Write a method that will take two int parameters and return a string which contains
     * sequential integers between the two given numbers.
     *
     * Examples:
     * printRange(2, 7) => "2 3 4 5 6 7"
     * printRange(16, 9) => "16 15 14 13 12 11 10 9"
     * printRange(8, 8) => "8"
     *
     * @param first number to begin the sequence.
     * @param second number to finish the sequence.
     * @return A string containing the sequence of integers separated by a space.
     */
    public String printRange(int first, int second){
        return "";
    }

    /**
     * Write a method which will accept an array of strings as a parameter.
     * The method will find the longest string and report back in the form of a string.
     *
     * For ties, take the latest longest string by that length.
     *
     * Example:
     * String[] strings = {"2", "test", "other", "6", "sighs"}
     * findLongestString(strings) => "The longest string is 'sighs'"
     *
     * @param strings list of strings to search through.
     * @return a pretty string which contains the longest string in the mentioned phrase.
     */
    public String findLongestString(String[] strings){
        return "";
    }

    /**
     * Write a method which accepts a string and an int as parameters.
     * The method should take each character of the string and duplicate it times number of times.
     *
     *
     * Examples:
     * canyonYell("test", 3) => "ttteeesssttt"
     * canyonYell("much long string this will be?", 0) => ""
     *
     * @param toYell The string to elongate.
     * @param times number of times the characters should replicate.
     * @return a string with duplicated characters.
     */
    public String canyonYell(String toYell, int times){
        return "";
    }

    /**
     * given 3 numbers, return true if one of the numbers is the midpoint of the other 2.
     * A number is the midpoint of two other numbers if it falls half way between those numbers.
     *
     * Example:
     *
     * hasMidPoint(2,6,4) => true
     * hasMidPoint(1,1,5) => false
     *
     * @param a the first of the numbers.
     * @param b the second of the numbers.
     * @param c the third of the numbers.
     * @return true if a, b, or c is the midpoint of the other two, false otherwise.
     */
    public boolean hasMidPoint(int a, int b, int c){
        return false;
    }

    /**
     * The digit range is defined as:
     * x = largest digit in number.
     * y = smallest digit in number.
     * range = x - y + 1
     *
     * Write a method which takes a number as a parameter and computes the digit range for the number.
     * Do not use string methods.
     *
     * @param num The number to use for the range finding.
     * @return the digit range of the number.
     */
    public int digitRange(int num){
        return -4;
    }

    /**
     * Given a string, report true or false if it contains only vowels.
     *
     * Example:
     * isAllVowels("cow") => false
     * isAllVowels("eiouuia") => true
     *
     * @param toTest The string to check for vowels.
     * @return true if the string contains only vowels, else false.
     */
    public boolean isAllVowels(String toTest){
        return false;
    }

    /**
     * Given the name of a file to open and a word length, write a method to report the number of words
     * longer than and equal to the given word length.
     *
     * Example:
     * file contents:"test the method for a accuracy measurement"
     * length: 4
     * The method should return 4
     *
     * @param name the name of the file to read.
     * @param length length of word to count.
     * @return number of words equal and longer in length than length.
     */
    public int countFileWordsOfLength(String name, int length){
        return -3;
    }
}
