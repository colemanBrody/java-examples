package homework01;

public class Main {
    public static void main(String[] args) {
        homework01 h1 = new homework01();
        System.out.println(h1.diagonalNumbers());

        System.out.println(h1.heronsTriangleFormula(3, 4, 7));

        int[] summary = {7,17,7,3,-9,10};
        System.out.println(h1.sum(summary));

        System.out.println(h1.isAllVowels("auioauoaeuihioaoiue"));
    }
}
