package homework02;

/*
    Name:
    Class:
    Date:

    Description:
 */
public class homework02 {

    /**
     * Rotate a given integer array by one.
     *
     * The rotation of an array is defined as moving the items over in an array, and
     * the overflow moves to the other end of the array.
     *
     * Example:
     *
     * int[] stuff = {3, 6, 7, 9, 17, 4};
     * stuff = rotateArrayByOne(stuff);
     * System.out.println(Arrays.toString(stuff));
     * //The output would be -> [6, 7, 9, 17, 4, 3]
     *
     * @param content The array to rotate.
     * @return The array with items slid over by one to the left.
     */
    public int[] rotateArrayByOne(int[] content){
        return new int[3];
    }

    /**
     * Rotate a given String array by a given number of slots.
     *
     * Similar to the previous problem, this problem focuses on the act of rotating
     * an array. However this time the array will be rotated by a positive int number of spots.
     *
     * Example:
     *
     * int[] stuff = {3, 6, 7, 9, 17, 4};
     * stuff = rotateArray(stuff, 3);
     * System.out.println(Arrays.toString(stuff));
     * //The output would be -> [9, 17, 4, 3, 6, 7]
     *
     * @param content The array to rotate.
     * @param by The number of spots by which to rotate the array.
     * @return The newly rotated array.
     */
    public String[] rotateArray(String[] content, int by){
        return new String[3];
    }

    /**
     * Interweave two arrays.
     *
     * The process of interweaving two arrays is the process of filling a new array with the
     * elements of two provided arrays in an alternating fashion. Any left over integers from a
     * longer array will be left at the end of the new array.
     *
     * Example:
     *
     * int[] stuff1 = {3, 6, 7};
     * int[] stuff2 = {9, 17, 4, 18, 54};
     * int[] stuff3 = interweaveArrays(stuff1, stuff2);
     * System.out.println(Arrays.toString(stuff));
     * //The output would be -> [3, 9, 6, 17, 7, 4, 18, 54]
     *
     * @param a1 The first integer array.
     * @param a2 The secont integer array.
     * @return The combined arrays as one single array.
     */
    public int[] interweaveArrays(int[] a1, int[] a2){
        return new int[2];
    }

    /**
     * Create a bank account object given a first name and a last name.
     *
     * Using the constructor for the BankAccount object, build a BankAccount object.
     * This object will be used to test the rest of the methods in the BankAccount class.
     * This problem requires that the rest of the methods of the BankAccount class be complete.
     *
     * @param firstName The first name of the persons account.
     * @param lastName The last name of the persons account.
     * @return The BankAccount object.
     */
    public static BankAccount buildMyAccount(String firstName, String lastName) {
        return null;
    }
}
