package homework02;

/*
    Name:
    Class:
    Date:

    Description:
 */
public class BankAccount {
    private String firstName;
    private String lastName;
    private double balance;

    public BankAccount(String first, String last){
        firstName = first;
        lastName = last;
        balance = 0.0;
    }

    public double checkBalance(){
        return 0.0;
    }

    public void deposit(double amount){

    }

    public double withdraw(double amount){
        return 0.0;
    }

    public void transfer(BankAccount to, double amount){

    }

    public String toString(){
        return "";
    }
}
